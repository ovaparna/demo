Backend Assignment

Application
We would like you to create a java based backend application.
It should contain the following REST endpoints:

List all stocks
List one stock
Update the price of a stock

The list of stocks should come from a memory database like h2.
The stock object contains at least the fields; id (Number), name (String), currentPrice (Amount) and lastUpdate (Timestamp).
Use the frameworks as you see fit to build and test this.

Implementation
Treat this application as a real MVP that should go to production.

Merge request
Provide us with a merge request to master of this repository.

Duration
This assignment should take 4-5 hours at the most.
Good luck!
